<?php
$cadena = $_POST['cadena'];
$nuevaCadena = preg_replace("/[^0-9\+\-\/\*]/", "", $cadena);
$signosSumaResta = array();
$signosMulDiv = array();
$partes = array();
$numeros = array();
$contador = 0;
$contadorF2 = 0;
$contadorSeparador = 0;
$haySignoSR = false;
echo "Ecuacion: ".$nuevaCadena."<br>";
echo "<br>";
// buscar signos + y -
for ($i=0; $i < strlen($nuevaCadena); $i++) {
  $signo = substr($nuevaCadena, $i , 1);
  if ($signo == "+") {
    $signosSumaResta[$contador][0] = "+";
    $signosSumaResta[$contador][1] = $i;
    $haySignoSR = true;
    $contador++;
  }elseif ($signo == "-") {
    $signosSumaResta[$contador][0] = "-";
    $signosSumaResta[$contador][1] = $i;
    $contador++;
    $haySignoSR = true;
  }
}

if ($haySignoSR == true) {
  // separando
  for ($i=0; $i < count($signosSumaResta); $i++) {
    if ($i == 0) {
      $partes[] = substr($nuevaCadena, 0, $signosSumaResta[$i][1]);
      $partes[] = substr($nuevaCadena,  $signosSumaResta[$i][1], 1);
    }else {
      $partes[] =substr($nuevaCadena, $signosSumaResta[$i-1][1] + 1, $signosSumaResta[$i][1] - $signosSumaResta[$i-1][1] -1);
      $partes[] =substr($nuevaCadena, $signosSumaResta[$i][1], 1);
    }
  }
    $ultimoSigno = count($signosSumaResta)-1;
    $partes[] = substr($nuevaCadena, $signosSumaResta[$ultimoSigno][1] + 1, strlen($nuevaCadena) - $signosSumaResta[$ultimoSigno][1] - 1);

  //OPERANDO PARTES DIVIDIDAS, FASE 2
  $unicoNumero = false;
  $haySignos = false;
   foreach ($partes as $key => $parte) { // $parte is string
     echo $parte ." ";
      if ($parte != "+" || $parte != "-") {
        for ($i=0; $i < strlen($parte); $i++) {
          $caracter = substr($parte, $i , 1);
          if ($caracter == "*") {
            $signosMulDiv[$contadorF2][0] = "*";
            $signosMulDiv[$contadorF2][1] = $i;
            $haySignos = true;
            $contadorF2++;
          }elseif ($caracter == "/") {
            $signosMulDiv[$contadorF2][0] = "/";
            $signosMulDiv[$contadorF2][1] = $i;
            $haySignos = true;
            $contadorF2++;
          }

        }
        if ($haySignos != false) {
          for ($i=0; $i < count($signosMulDiv); $i++) {
            if ($i == 0) {
              //guarda lo que esta antes del primer signo, si no hay nada error xd
              if ($signosMulDiv[$i][1] == "") {
                echo "Error de sintaxis :( <br>";
                errorXD();
              }
              $numeros[] = substr($parte, 0, $signosMulDiv[$i][1]);
              $numeros[] = substr($parte,  $signosMulDiv[$i][1], 1);
            }else {
              // lo mismo para antes de los demas signos
              if (substr($parte, $signosMulDiv[$i-1][1] + 1, $signosMulDiv[$i][1] - $signosMulDiv[$i-1][1] -1) == "") {
                echo "jaja, nel chavo. ERROR";
                errorXD();
              }
              $numeros[] =substr($parte, $signosMulDiv[$i-1][1] + 1, $signosMulDiv[$i][1] - $signosMulDiv[$i-1][1] -1);
              $numeros[] =substr($parte, $signosMulDiv[$i][1], 1);
            }
          }
            $variable = count($signosMulDiv);
            //echo "cantidad de signos: ".$variable."<br>";
            $posicionUltimoSignoArreglo = $variable-1;
            $posicionSigno = $signosMulDiv[$posicionUltimoSignoArreglo][1];
            //echo "Pos. ".$posicionSigno."<br>";
            $posicionDespuesUltimoSigno = $signosMulDiv[$posicionUltimoSignoArreglo][1] + 1;
            $largoUltimo = strlen($parte) - $signosMulDiv[$posicionUltimoSignoArreglo][1] - 1;
            //echo "La posicion del ultimo signo en el arreglo es: ".$posicionUltimoSignoArreglo."<br> posicionDespuesUltimoSigno: ".$posicionDespuesUltimoSigno." largoUltimo ". $largoUltimo."<br>";
            $numeros[] = substr($parte, $posicionDespuesUltimoSigno, $largoUltimo);
            // operar
            for ($e=0; $e < count($numeros); $e++) {
              if ($numeros[$e] == "*") {
                $resultado = $numeros[$e-1] * $numeros[$e + 1];
                $numeros[$e+1] = $resultado;
              }elseif ($numeros[$e] == "/") {
                $resultado = $numeros[$e-1] / $numeros[$e + 1];
                $numeros[$e+1] = $resultado;
              }
            }
            $posResultado = count($numeros)-1;
            echo "= ".$numeros[$posResultado]. "<br>";
            $partes[$key] = $numeros[$posResultado];
        }else {
          if ($parte == "+" || $parte == "-") {
            echo "<br>";
          }elseif ($parte != "") {
            $partes[$key] = $parte;
            echo "= " . $parte . " <br>";
          } else {
            evaluarMultiplicacionDivision($nuevaCadena);
          }
        }
        foreach($signosMulDiv as $key => $value){
              //echo "......Signo: ".$signosMulDiv[$key][0].",  ";
          		unset($signosMulDiv[$key][0]);
              //echo "Pos. ". $signosMulDiv[$key][1]."<br>";
              unset($signosMulDiv[$key][1]);
          }
        foreach($numeros as $key => $value){
          		unset($numeros[$key]);
              //unset($numeros[$key]);
          }
          $contadorF2 = 0;
          $signosMulDiv = array();
          $numeros = array();
    }else {
      //echo $parte. "<br>";
    }
    $haySignos = false;
    echo "<br>";
  }
  for ($e=0; $e < count($partes) ; $e++) {
    if ($partes[$e] == "+") {
      $resultado = $partes[$e-1] + $partes[$e + 1];
      $partes[$e+1] = $resultado;
    }elseif ($partes[$e] == "-") {
      $resultado = $partes[$e-1] - $partes[$e + 1];
      $partes[$e+1] = $resultado;
    }
  }
  $posResultadoFinal = count($partes)-1;
  echo "RESULTADO: ".$partes[$posResultadoFinal]. "<br>";
  exitXD();

}else {
  evaluarMultiplicacionDivision($nuevaCadena);
}

// FUNCIONES

function errorXD(){
  exit();
}
function evaluarMultiplicacionDivision($operacion)
{
  $signosMD = array();
  $numerosMD = array();
  $haysig = false;
  $contadorFE = 0;
  for ($i=0; $i < strlen($operacion); $i++) {
    $caracter = substr($operacion, $i , 1);
    if ($caracter == "*") {
      $signosMD[$contadorFE][0] = "*";
      $signosMD[$contadorFE][1] = $i;
      $haysig = true;
      $contadorFE++;
    }elseif ($caracter == "/") {
      $signosMD[$contadorFE][0] = "/";
      $signosMD[$contadorFE][1] = $i;
      $contadorFE++;
      $haysig = true;
    }
  }
  if ($haysig) {
    for ($i=0; $i < count($signosMD); $i++) {
      if ($i == 0) {
        //guarda lo que esta antes del primer signo, si no hay nada error xd
        if ($signosMD[$i][1] == "") {
          echo "Error de sintaxis :( <br>";
          errorXD();
        }
        $numerosMD[] = substr($operacion, 0, $signosMD[$i][1]);
        $numerosMD[] = substr($operacion,  $signosMD[$i][1], 1);
      }else {
        // lo mismo para antes de los demas signos
        if (substr($operacion, $signosMD[$i-1][1] + 1, $signosMD[$i][1] - $signosMD[$i-1][1] -1) == "") {
          echo "jaja, nel chavo. ERROR";
          errorXD();
        }
        $numerosMD[] =substr($operacion, $signosMD[$i-1][1] + 1, $signosMD[$i][1] - $signosMD[$i-1][1] -1);
        $numerosMD[] =substr($operacion, $signosMD[$i][1], 1);
      }
    }
     $pos = count($signosMD)-1;
     $posUltimoSigno = $signosMD[$pos][1];
     $posDespuesSigno = $posUltimoSigno + 1;
     $lUltimo = strlen($operacion) - $signosMD[$posUltimoSigno][1] - 1;
     //echo "La posicion del ultimo signo en el arreglo es: ".$posicionUltimoSignoArreglo."<br> posicionDespuesUltimoSigno: ".$posicionDespuesUltimoSigno." largoUltimo ". $largoUltimo."<br>";
     $numerosMD[] = substr($operacion, $posDespuesSigno, $lUltimo);
     // operar
    for ($e=0; $e < count($numerosMD); $e++) {
      if ($numerosMD[$e] == "*") {
        $res = $numerosMD[$e-1] * $numerosMD[$e + 1];
        $numerosMD[$e+1] = $res;
      }elseif ($numerosMD[$e] == "/") {
        $res = $numerosMD[$e-1] / $numerosMD[$e + 1];
        $numerosMD[$e+1] = $res;
      }
    }
    $posRes = count($numerosMD)-1;
    echo "= ".$numerosMD[$posRes]. "<br>";
    //$partes[$key] = $numeros[$posResultado];
  }

}
?>
